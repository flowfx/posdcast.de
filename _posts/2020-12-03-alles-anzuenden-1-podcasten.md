---
title: 'Alles anzünden! #1 Podcasten'
subtitle: mit Nathan
datum: Dec 3rd, 2020
layout: post
author: Florian Posdziech
explicit: 'yes'
duration: '0:39:51.571'
audio:
  mp3: pos001.mp3
filesize:
  mp3: 33509708
---

<!---
The filesize block above can be deleted, if your audio files are hosted within the episodes directory.
It is only necessary for hosting remotely.
-->

{% podigee_player page %}

## Links

- [Ultraschall](https://ultraschall.fm/) (Software)
- [Reaper](https://www.reaper.fm/) (Software)
- [Chiemseer Hell](https://chiemseer-erleben.de/) (Bier)
- [Codestammtisch](https://codestammtis.ch/) (Podcast)
- [Auphonic](https://auphonic.com/) (Magie as a service)
- [Several Ways To Live](https://severalwaystolive.com/) (Podcast)
- [Cultural Comments](https://podcast.c3s.cc/) (Podcast)
- [Stardew Valley](https://www.stardewvalley.net/) (Spiel)
- [bullenscheisse](https://bullenscheisse.de/) (Blog)
- [Allein zuhaus](https://alleinzu.haus/) (Podcast)
- [Steganographie](https://de.wikipedia.org/wiki/Steganographie) (Kunst oder Wissenschaft)
- [Tacos und Limetten](https://tacosundlimetten.de/) (Podcast)
- [C3S](https://www.c3s.cc/) (GEMA-Alternative)
- [Tim Pritlove](https://metaebene.me/timpritlove/) (Podcaster)
- [The Poscast](https://theathletic.com/podcast/174-the-poscast/) (Podcast)
